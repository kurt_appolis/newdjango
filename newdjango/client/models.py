"""Client models."""

from django.db import models
from django.urls import reverse


class Person(models.Model):
    """Person model."""

    first_name = models.CharField(max_length=250)
    last_name = models.CharField(max_length=100)

    age = models.IntegerField(default=16)
    id_number = models.CharField(max_length=13, default=None, help_text="RSA ID number")
    is_first_user = models.BooleanField(default=False,
                                        help_text="Is this your\
        first time using the system?")

    contact_number = models.CharField(max_length=250,
                                      default=None,
                                      null=True,
                                      blank=True)
    GROUP_NAMES = ((0, 'Group 1'),
                   (1, 'Group 2'),
                   (2, 'Group 3'))
    group = models.IntegerField(default=0, choices=GROUP_NAMES)

    profile_pic = models.ImageField(upload_to="person/profile_pics/",
                                    default=None,
                                    null=True,
                                    blank=True)

    timestamp = models.DateTimeField(auto_now_add=True)
    timestamp_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        """To string function."""
        return "{} {}".format(self.first_name, self.last_name)

    def get_absolute_url(self):
        """Return to a url after inserting."""
        return reverse('client:list-person', kwargs={})


class Car(models.Model):
    """Car class that belongs to person."""

    person = models.ForeignKey(Person, on_delete=models.CASCADE)

    vin_number = models.CharField(max_length=250)
    engine_number = models.CharField(max_length=250)

    timestamp = models.DateTimeField(auto_now_add=True)
    timestamp_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        """To string function."""
        return "{} {}".format(self.person, self.engine_number)
