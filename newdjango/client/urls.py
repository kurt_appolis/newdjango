"""Client URL Configuration."""

from django.urls import path
from .views import (HomePageTemplateView,
                    PersonListView,
                    PersonCreateView,
                    PersonUpdateView,
                    PersonDetailView,
                    PersonDeleteView)

app_name = "client"


urlpatterns = [
    path('', HomePageTemplateView.as_view(), name="home"),
    path('person/list/', PersonListView.as_view(), name="list-person"),
    path('person/create/', PersonCreateView.as_view(), name="create-person"),
    path('person/update/<pk>/', PersonUpdateView.as_view(), name="update-person"),
    path('person/<pk>/', PersonDetailView.as_view(), name="detail-person"),
    path('person/<pk>/delete/', PersonDeleteView.as_view(), name="delete-person")
]
