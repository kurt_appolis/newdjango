"""Client view classes."""
from django.urls import reverse_lazy
from django.views.generic import (TemplateView,
                                  ListView,
                                  DetailView)
from django.views.generic.edit import (CreateView,
                                       UpdateView,
                                       DeleteView)
from .models import (Person)


class HomePageTemplateView(TemplateView):
    """Home page templateview."""

    template_name = 'client/homepage.html'


class PersonListView(ListView):
    """Person list view."""

    model = Person


class PersonCreateView(CreateView):
    """Person create view."""

    model = Person
    fields = ['first_name',
              'last_name',
              'id_number']


class PersonUpdateView(UpdateView):
    """Person update view."""

    model = Person
    fields = ['first_name',
              'age',
              'is_first_user',
              'group',
              'last_name',
              'profile_pic']


class PersonDetailView(DetailView):
    """Person detail view."""

    model = Person


class PersonDeleteView(DeleteView):
    """Person delete view."""

    model = Person
    success_url = reverse_lazy('client:list-person')
