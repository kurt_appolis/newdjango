# Generated by Django 2.1.7 on 2019-04-01 10:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0006_person_profile_pic'),
    ]

    operations = [
        migrations.AddField(
            model_name='person',
            name='contact_number',
            field=models.CharField(blank=True, default=None, max_length=250, null=True),
        ),
    ]
