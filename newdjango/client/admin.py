"""Client admin classess."""

from django.contrib import admin
from .models import (Person,
                     Car)


@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    """Person admin."""

    search_fields = ('first_name', 'last_name')
    list_display = ('first_name', 'last_name', 'timestamp', 'timestamp_updated')
    list_filter = ('age', 'timestamp', 'timestamp_updated')


@admin.register(Car)
class CarAdmin(admin.ModelAdmin):
    """Car admin."""

    list_display = ('pk', 'person', 'engine_number')
