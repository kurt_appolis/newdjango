# Generated by Django 2.1.7 on 2019-04-01 10:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('register', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='attendance',
            name='checkin_time',
            field=models.DateTimeField(blank=True, default=None, null=True),
        ),
    ]
