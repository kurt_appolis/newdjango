"""Register admin views."""

from django.contrib import admin
from .models import Attendance


@admin.register(Attendance)
class AttendanceAdmin(admin.ModelAdmin):
    """Attendance admin."""

    list_display = ('pk', 'person', 'checkout_time', 'checkin_time')
