"""Register models."""

from django.db import models
from client.models import (Person)


class Attendance(models.Model):
    """Atttendance model."""

    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    checkin_time = models.DateTimeField(null=True,
                                        blank=True,
                                        default=None)
    checkout_time = models.DateTimeField(null=True,
                                         blank=True,
                                         default=None)

    comment = models.TextField(max_length=2500,
                               default=None,
                               blank=True,
                               null=True,
                               help_text="Add in an optional comment")

    timestamp_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        """To string model."""
        return "{}".format(self.person, self.checkin_time)
