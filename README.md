# README #

A basic project doing CRUD on the front end.


### How do I get set up? ###

> cd ./newdjango

> virtualenv -p python3 ./env

> source ./env/bin/activate

> pip install -r ./requirements.txt

> cd ./newdjango

> ./manage.py makemigrations && ./manage.py migrate

> ./manage.py createsuperuser


### Project Owners

- Kurt Appolis (Tech Lead) - kurt@rlabs.org

